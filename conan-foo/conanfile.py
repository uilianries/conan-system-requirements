from conans import ConanFile, CMake
from conans.tools import SystemPackageTool


class FooConan(ConanFile):
   name = "foo"
   version = "0.1.0"
   license= "MIT"
   generators = "cmake"
   exports_sources = ["CMakeLists.txt", "foo/*"]
   options = {"shared": [True, False]}
   settings = "os", "compiler", "build_type", "arch"
   default_options = "shared=True"

   def system_requirements(self):
       if self.settings.os == "Linux":
           package_tool = SystemPackageTool()
           package_tool.install(packages="htop", update=True)

   def build(self):
       cmake = CMake(self.settings)
       shared = {"BUILD_SHARED_LIBS" : self.options.shared }
       cmake.configure(self, source_dir=self.conanfile_directory, defs=shared)
       cmake.build(self)

   def package(self):
       self.copy("*.hpp", dst="include", src="foo/include")
       self.copy("*.so*", dst="lib", keep_path=False)
       self.copy("*.a", dst="lib", keep_path=False)

   def package_info(self):
       self.cpp_info.libs = ["foo"]
