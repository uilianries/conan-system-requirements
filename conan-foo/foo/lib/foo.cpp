#include "foo.hpp"

#include <sstream>

std::string Foo::show() const {
    std::ostringstream oss;
    oss << __FILE__ << ":" << __LINE__ << " - " << __func__;
    return oss.str();
}
