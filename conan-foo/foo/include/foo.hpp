#ifndef FOO_HPP
#define FOO_HPP

#include <string>

class Foo {
public:
    std::string show() const;
};

#endif // FOO_HPP
