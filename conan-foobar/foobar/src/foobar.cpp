#include <iostream>
#include "bar.hpp"

int main() {
    Bar bar;
    std::cout << bar.show_foo() << std::endl;
    return 0;
}
