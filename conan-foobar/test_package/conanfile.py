from conans import ConanFile, CMake
from os import getenv


class TestFoobarConan(ConanFile):
    name = "test-foobar"
    settings = "os", "compiler", "build_type", "arch"
    channel = getenv("CONAN_CHANNEL", "testing")
    user = getenv("CONAN_USERNAME", "uilianries")
    requires = "foobar/0.1.0@%s/%s" % (user, channel)
    generators = "cmake"

    def imports(self):
        self.copy(pattern="foobar", dst="bin", src="bin")

    def test(self):
        self.run("bin/foobar")
