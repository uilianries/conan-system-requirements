from conans import ConanFile, CMake
from conans.tools import SystemPackageTool


class FoobarConan(ConanFile):
    name = "foobar"
    version = "0.1.0"
    license= "MIT"
    generators = "cmake"
    exports_sources = ["CMakeLists.txt", "foobar/*"]
    settings = "os", "compiler", "build_type", "arch"
    requires = "bar/0.1.0@uilianries/testing"

    def build(self):
        cmake = CMake(self.settings)
        cmake.configure(self, source_dir=self.conanfile_directory)
        cmake.build(self)

    def package(self):
        self.copy(pattern="foobar", dst="bin", src="bin")
