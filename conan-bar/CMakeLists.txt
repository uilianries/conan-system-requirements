cmake_minimum_required(VERSION 2.8)
project(bar CXX)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

include_directories(${CMAKE_PROJECT_NAME}/include)
file(GLOB SOURCE_FILES ${CMAKE_PROJECT_NAME}/lib/*.cpp)

add_library(${CMAKE_PROJECT_NAME} ${SOURCE_FILES})
target_link_libraries(${CMAKE_PROJECT_NAME} ${CONAN_LIBS})

install(TARGETS ${CMAKE_PROJECT_NAME}
        DESTINATION ${CMAKE_INSTALL_PREFIX}/lib)
