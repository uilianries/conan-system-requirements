#include "bar.hpp"
#include "foo.hpp"

std::string Bar::show_foo() const {
    Foo foo;
    return foo.show();
}
