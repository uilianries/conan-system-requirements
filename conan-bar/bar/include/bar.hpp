#ifndef BAR_HPP
#define BAR_HPP

#include <string>

class Bar {
public:
    std::string show_foo() const;
};

#endif // BAR_HPP
