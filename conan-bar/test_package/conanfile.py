from conans import ConanFile, CMake
from os import getenv


class TestBarConan(ConanFile):
    name = "test-bar"
    settings = "os", "compiler", "build_type", "arch"
    channel = getenv("CONAN_CHANNEL", "testing")
    user = getenv("CONAN_USERNAME", "uilianries")
    requires = "bar/0.1.0@%s/%s" % (user, channel), "gtest/1.7.0@lasote/stable"
    generators = "cmake"

    def build(self):
        cmake = CMake(self.settings)
        cmake.configure(self, source_dir=self.conanfile_directory, build_dir="./")
        cmake.build(self)

    def test(self):
        cmake = CMake(self.settings)
        cmake.configure(self, source_dir=self.conanfile_directory, build_dir="./")
        cmake.build(self, target="test")
